/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "StringUtil.h"
#include <string.h>

bool isMirror(const char* word)
{
    bool result = true;
    for(int i = 0, j = strlen(word) -1; i < j; i++, j--)
    {
        if(word[i] != word[j]){
            result = false;
        }
    }
    return result;
}