/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StringUtil.h
 * Author: avincze
 *
 * Created on September 27, 2018, 11:50 AM
 */

#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <stdbool.h>

bool isMirror(const char* word);

#endif /* STRINGUTIL_H */

