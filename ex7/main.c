/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 25, 2018, 10:54 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


int accum(int* array, int length, int init)
{
    for(int i = 0; i < length; i++)
    {
        init+=i[array];
    }
    return init;
}
/*
 * 
 */

typedef int (*Policy) (int, int);
int accum2(int* array, int length, int init, Policy operation)
{
    for(int i = 0; i < length; i++)
    {
        init = operation(init, i[array]);
    }
    return init;
}

typedef bool (*Predicate) (int);
int accum3(int* begin, int* end, int init, 
        Policy operation, Predicate condition)
{
    for(; begin != end; begin++)
    {
        if(condition(*begin)){
            init = operation(init, *begin);
        }
    }
    return init;
}

int add(int a, int b)
{
    return a + b;
}

int multiply(int a, int b)
{
    return a * b;
}

bool positive(int a)
{
    return a > 0;
}

bool even(int a)
{
    return a % 2 == 0;
}
int main(int argc, char** argv) {
    int a[]={1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum(a, 10, 0));
    
    printf("%d\n", accum2(a, 10, 0, &add));
    printf("%d\n", accum2(a, 10, 1, multiply));
    
    printf("%d\n", accum3(a, a + 10, 0, add, even));
    printf("%d\n", accum3(a, a + 10, 1, multiply, positive));
    
    return (EXIT_SUCCESS);
}

