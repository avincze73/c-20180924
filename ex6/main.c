/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2018. szeptember 25., 9:44
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void freeNumbers(int** p) {
    for (int i = 0; i < 4; i++) {
        free(*(p + i));
    }
    free(p);
}

int** getNumbers() {
    int** result = (int**) malloc(4 * sizeof (int*));

    *result = (int*) malloc(1 * sizeof (int));
    **result = 1;

    *(result + 1) = (int*) malloc(2 * sizeof (int));
    **(result + 1) = 2;
    *(*(result + 1) + 1) = 3;

    *(result + 2) = (int*) malloc(3 * sizeof (int));
    **(result + 2) = 4;
    *(*(result + 2) + 1) = 5;
    *(*(result + 2) + 2) = 6;


    *(result + 3) = (int*) malloc(4 * sizeof (int));
    **(result + 3) = 7;
    *(*(result + 3) + 1) = 8;
    *(*(result + 3) + 2) = 9;
    *(*(result + 3) + 3) = 10;

    return result;
}

void getWeekDays(char*** days) {
    const char* d[] = 
    {"sun","mon","tue", "wed", "thu", "fri", "sat"};
    *days = malloc(sizeof (char*)*7);
    for (int i = 0; i < 7; i++) {
        (*days)[i] = malloc(strlen(d[i]) + 1);
        strcpy((*days)[i], d[i]);
    }
}

void freeWeekDays(char** days) {
    for (int i = 0; i < 7; i++) {
        free(days[i]);
    }
    free(days);
}

/*
 * 
 */
int main(int argc, char** argv) {
    int** numbers = getNumbers();
    //printf("%d", *(*(numbers + 2) + 0));
    freeNumbers(numbers);

    char ** days = NULL;
    getWeekDays(&days);
    printf("%s", days[0]);
    freeWeekDays(days);
    return (EXIT_SUCCESS);
}

