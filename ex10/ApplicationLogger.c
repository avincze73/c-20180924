/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//https://gitlab.com/avincze73/c-20180924
//ex10

#include <stddef.h>
#include <stdbool.h>

#include "ApplicationLogger.h"

static MyLogger registry[] = 
{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

void LOG(const char* message, Severity severity) {
    for(int i = 0; i != 10; i++)
    {
        if(NULL != i[registry])
        {
            i[registry](message, severity);
        }
    }
}

void attach(MyLogger myLogger) {
    bool found = false;
    for (int i = 0; i != 10; i++) {
        if (registry[i] == myLogger) {
            found = true;
            break;
        }
    }
    if (!found) {
        for (int i = 0; i != 10; i++) {
            if ( NULL == i[registry] ) {
                registry[i] = myLogger;
                break;
            }
        }
    }
}

void detach(MyLogger myLogger) {
    for (int i = 0; i != 10; i++) {
        if(registry[i]==myLogger){
            registry[i] = NULL;
        }
    }
}

