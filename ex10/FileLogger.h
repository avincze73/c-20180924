/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileLogger.h
 * Author: avincze
 *
 * Created on September 27, 2018, 10:06 AM
 */

#ifndef FILELOGGER_H
#define FILELOGGER_H

#include "Types.h"
void file_logger(const char*, Severity);

#endif /* FILELOGGER_H */

