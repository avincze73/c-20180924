/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConsoleLogger.h
 * Author: avincze
 *
 * Created on September 27, 2018, 10:01 AM
 */

#ifndef CONSOLELOGGER_H
#define CONSOLELOGGER_H
#include "Types.h"
void console_logger(const char*, Severity);

#endif /* CONSOLELOGGER_H */

