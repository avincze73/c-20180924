/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 27, 2018, 9:55 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "ApplicationLogger.h"
#include "ConsoleLogger.h"
#include "FileLogger.h"
#include "SocketLogger.h"

/*
 * 
 */
int main(int argc, char** argv) {
    attach(console_logger);
    LOG("first message", INFO);
    
    attach(file_logger);
    LOG("second message", INFO);
    
    attach(socket_logger);
    LOG("third message", INFO);
    
    detach(socket_logger);
    LOG("fourth message", INFO);
    return (EXIT_SUCCESS);
}

