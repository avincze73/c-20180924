/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Types.h
 * Author: avincze
 *
 * Created on September 27, 2018, 9:59 AM
 */

#ifndef TYPES_H
#define TYPES_H

typedef enum 
{
    INFO, ERROR
} Severity;

typedef void (*MyLogger)(const char*, Severity);

#endif /* TYPES_H */

