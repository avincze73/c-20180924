/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ApplicationLogger.h
 * Author: avincze
 *
 * Created on September 27, 2018, 9:56 AM
 */

#ifndef APPLICATIONLOGGER_H
#define APPLICATIONLOGGER_H

#include "Types.h"
void LOG(const char* message, Severity severity);
void attach(MyLogger myLogger);
void detach(MyLogger myLogger);


#endif /* APPLICATIONLOGGER_H */

