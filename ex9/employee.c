/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "employee.h"
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

struct Employee {
    char name[50];
    Address address;
    int salary;
};

static EmployeePtr* database = NULL;
static int size = 0;
static int capacity = 0;

static EmployeePtr create(const char* name, Address* address, int salary)
{
    EmployeePtr result = (EmployeePtr) malloc(sizeof(struct Employee));
    result->salary = salary;
    strcpy(result->name, name);
    result->address.zip = address->zip;
    result->address.city = 
            malloc(sizeof(char)*(strlen(address->city) + 1));
    strcpy(result->address.city, address->city);
    result->address.street = 
            malloc(sizeof(char)*(strlen(address->street) + 1));
    strcpy(result->address.street, address->street);
    return result;
}


EmployeePtr insert(const char* name, Address* address, int salary)
{
    if (size == capacity) {
        capacity = 2 * size + 1;
        EmployeePtr* db = malloc(sizeof(EmployeePtr)*capacity);
        for(int i = 0; i < size; ++i)
        {
            db[i] = database[i];
        }
        free(database);
        database = db;
    }
    database[size++] = create(name, address, salary);
}
 

void print(struct Employee* employee)
{
    printf("Name: %s\nAddress: %d %s, %s\nSalary: %d\n", 
            employee->name, 
            employee->address.zip,
            employee->address.city,
            employee->address.street,
            employee->salary);
}

void printAll()
{
    for(int i = 0; i != size; i++)
    {
        print(database[i]);
    }
}

