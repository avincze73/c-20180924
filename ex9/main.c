/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 25, 2018, 1:35 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "employee.h"

/*
 * 
 */
int main(int argc, char** argv) {

    EmployeePtr
    employee = insert(
            "aaa",
            &(Address){1111, "city1", "street1"},
    1000);

    employee = insert(
            "bbb",
            &(Address){2222, "city2", "street2"},
    2000);
    printf("printing individual employee\n");
    print(employee);
    printf("printing all employees\n");
    printAll();

    return (EXIT_SUCCESS);
}

