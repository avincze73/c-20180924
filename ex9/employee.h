/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   employee.h
 * Author: avincze
 *
 * Created on September 25, 2018, 1:35 PM
 */

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

typedef struct {
    int zip;
    char* city;
    char* street;
} Address;
struct Employee;
typedef struct Employee* EmployeePtr;
void printAll();
void print(EmployeePtr);
EmployeePtr insert(const char* name, Address* address, int salary);

#endif /* EMPLOYEE_H */

