/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "rw.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

static void init(char matrix[][10]) {
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            matrix[i][j] = '.';
        }
    }
}
void randomwalk(char matrix[][10]) {
    srand(time(NULL));
    init(matrix);
    int x = 5, y = 5;
    char lastchar = 'a';
    matrix[5][5] = lastchar;
    int randomnumber;
    while (1) {
        randomnumber = rand() % 4;
        int oldx = x,  oldy = y;
        switch (randomnumber) {
            case 0: --y; break;
            case 1: --x; break;
            case 2: ++y; break;
            case 3: ++x; break;
        }
        if (matrix[x][y] != '.') {
            x = oldx;
            y = oldy;
            continue;
        }
        if (x == 10 || y == 10 || x == -1 || y == -1) {
            break;
        }
        oldx = x;
        oldy = y;
        matrix[x][y] = ++lastchar;
    }
}
void show(char matrix[][10]) {
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            printf("%c ", matrix[i][j]);
        }
        printf("\n");
    }
}
