/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on September 25, 2018, 11:54 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int stringCompare(const void* a, const void* b) {
    return strcmp((const char*) a, (const char*) b);
}

bool isAnagram(const char* a, const char* b) {
    if (strlen(a) != strlen(b)) {
        return false;
    }
    char aa[strlen(a) + 1];
    strcpy(aa, a);
    char bb[strlen(b) + 1];
    strcpy(bb, b);
    qsort(aa, strlen(aa), sizeof (char), stringCompare);
    qsort(bb, strlen(bb), sizeof (char), stringCompare);
    return strcmp(aa, bb) == 0;
}

/*
 * 
 */
int main(int argc, char** argv) {
    printf("%s", isAnagram("abcd", "bacd") ? "true" : "false");

    return (EXIT_SUCCESS);
}

